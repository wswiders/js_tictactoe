var HistoryModule = (function () {
    var timesXWon = 0,
        timesOWon = 0,
        timesWasDraw = 0;

    return {
        xWon: function () {
            timesXWon++;
        },
        oWon: function () {
            timesOWon++;
        },
        wasDraw: function () {
            timesWasDraw++;
        },
        getTimesXWon: function () {
            return timesXWon;
        },
        getTimesOWon: function () {
            return timesOWon;
        },
        getTimesWasDraw: function () {
            return timesWasDraw;
        }
    };
})();
