var BoardBuiderModule = (function() {

    var boardSize = 3;

    function setBoardMessage(textBox, msg) {
        textBox.innerText = msg;
    }

    function increaseSizeAndSetMessage(textBox) {
        ++boardSize;
        if (boardSize > 7) {
            boardSize = 7;
            alert('Board size cant be bigger than 7x7');
        }
            setBoardMessage(textBox, 'Next round will sart with board size ' + boardSize + 'x' + boardSize);
    }

    function decreaseSizeAndSetMessage(textBox) {
        --boardSize;
        if (boardSize < 3) {
            boardSize = 3;
            alert('Board size cant be smaller than 3x3');
        }
            setBoardMessage(textBox, 'Next round will sart with board size ' + boardSize + 'x' + boardSize);
    }

    function getBoardSize() {
        return boardSize;
    }

    function getBoard() {
        var board = [];
        for (var i = 0; i < boardSize; i++) {
            for (var j = 0; j < boardSize; j++) {
                board[i * boardSize + j] = '';
            }
        }
        return board;
    }

    function setBoardSizeOnPage(board) {
        board.style.width = 100 * boardSize;
        board.style.height = 100 * boardSize;
    }

    function resetBoardSize() {
        boardSize = 3;
    }

    return {
        resetBoardSize: resetBoardSize,
        increaseSizeAndSetMessage: increaseSizeAndSetMessage,
        decreaseSizeAndSetMessage: decreaseSizeAndSetMessage,
        getBoardSize: getBoardSize,
        getBoard: getBoard,
        setBoardSizeOnPage: setBoardSizeOnPage
    };
})();
