var WinnerCheckerModule = (function() {

    function getListOfPossibleWinsCombination(boardSize, board) {
        usedMarksInPosition = [];
        var diagonal = [];
        for (var i = 0; i < boardSize; i++) {
            var row = [];
            var column = [];
            for (var j = 0; j < boardSize; j++) {
                row.push(board[(i * boardSize) + j]);
                column.push(board[(j * boardSize) + i]);
                if (i == j) {
                    diagonal.push(board[(i * boardSize) + j]);
                }
            }
            usedMarksInPosition.push(row);
            usedMarksInPosition.push(column);
        }
        usedMarksInPosition.push(diagonal);
        usedMarksInPosition.push(getReverseDiagonal(boardSize, board));
        return usedMarksInPosition;
    }

    function getReverseDiagonal(boardSize, board) {
        var reverseDiagonal = [];
        for (i = boardSize - 1, j = 0; i >= 0; i--, j++) {
            reverseDiagonal.push(board[(j * boardSize) + i]);
        }
        return reverseDiagonal;
    }

    function isDraw(boardSize, board) {
        for (var i = 0; i < boardSize * boardSize; i++) {
            if (board[i] == '') {
                return false;
            }
        }
        return true;
    }

    function includes(array, variable) {
        for (var i = 0; i < array.length; i++) {
            if (array[i] == variable) {
                return true;
            }
        }
        return false
    }

    function checkWinner(boardSize, board) {
        var listOfPossibleWinsCombination = getListOfPossibleWinsCombination(boardSize, board);
        if (isDraw(boardSize, board)) {
            return '-';
        }
        for (var i = 0; i < listOfPossibleWinsCombination.length; i++) {
            var possiblePossition = listOfPossibleWinsCombination[i];
            if (!includes(possiblePossition, '')) {
                firstMark = possiblePossition[0];
                if ((firstMark == 'X' && !includes(possiblePossition, 'O')) ||
                    (firstMark == 'O' && !includes(possiblePossition, 'X'))) {
                    return firstMark;
                }
            }
        }
        return '';
    }

    return {
        checkWinner: checkWinner
    };
})();
