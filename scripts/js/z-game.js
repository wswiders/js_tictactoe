var GameModule = (function(boardBuider, history, winnerChecker) {

    var boardSize;
    var board = [];
    var turn = 'X';
    var firstMove = 'X';
    var isFirstMoveDone = false;
    var isGameStarted = true;

    function setFirstPlayerMessage() {
        document.getElementById("newGameInfoText").innerText = 'New game will start player ' + firstMove;
    }

    function setTurnMessage() {
        document.getElementById("turnInfoText").innerText = "It's player " + turn + " turn. ";
    }

    function initGameBoard() {
        var text = '';
        for (var square in board) {
            text += '<div class="gridsquare" id="' + square +
                '" onmousedown="GameModule.setMarkerAndChangeTurn(this);" onmouseup="GameModule.checkWinAndAddPoints();"></div>';
        }
        document.getElementById("gameBoard").innerHTML = text;
        boardBuider.setBoardSizeOnPage(document.getElementById("gameBoard"));
    }

    function initGame() {
        boardSize = boardBuider.getBoardSize();
        board = boardBuider.getBoard();
        isGameStarted = true;
        isFirstMoveDone = false;
        turn = firstMove;
        setFirstPlayerMessage();
        setTurnMessage();
        initGameBoard();
    }

    function increaseBoardSizeAndSetMessage() {
        var textBox = document.getElementById("nextRoundSizeInfo");
        boardBuider.increaseSizeAndSetMessage(textBox);
    }

    function decreaseBoardSizeAndSetMessage() {
        var textBox = document.getElementById("nextRoundSizeInfo");
        boardBuider.decreaseSizeAndSetMessage(textBox);
    }

    function setMarkerAndChangeTurn(square) {
        if (isGameStarted) {
            var id = square.getAttribute('id');
            if (board[id] == '') {
                isFirstMoveDone = true;
                board[id] = turn;
                square.innerText = turn;
                setColor(square);
                changeTurn();
            }
        }
    }

    function setColor(square){
      if(turn=='X'){
        square.style.color = 'green';
      } else{
        square.style.color = 'red';
      }
    }

    function changeTurn() {
        turn = (turn == 'X') ? 'O' : 'X';
        setTurnMessage();
    }

    function changeFirstPlayer() {
        firstMove = (firstMove == 'X') ? 'O' : 'X';
        setFirstPlayerMessage();
    }

    function checkWinAndAddPoints() {
        if (isGameStarted) {
            var winner = winnerChecker.checkWinner(boardSize, board);
            if (winner != '') {
                isGameStarted = false;
                if (winner == 'X') {
                    history.xWon();
                    alert('Player ' + winner + ' win the match!');
                } else if (winner == 'O') {
                    history.oWon();
                    alert('Player ' + winner + ' win the match!');
                } else {
                    history.wasDraw();
                    alert('Game ended in draw.');
                }
                document.getElementById("turnInfoText").innerText = 'Waiting for match reset.';
                document.getElementById("scoreText").innerText = history.getTimesXWon() + ' - ' + history.getTimesWasDraw() + ' - ' + history.getTimesOWon();
            }
        }
    }

    function getTurn() {
        return turn;
    }

    function getFirstTurn() {
        return firstMove;
    }

    function setFirstTurn(turn){
      firstMove = turn;
    }

    function getBoard(){
      return board;
    }

    function getIsFirstMoveDone(){
      return isFirstMoveDone;
    }

    function getIsGameStarted(){
      return isGameStarted;
    }

    return {
        initGame: initGame,
        increaseBoardSizeAndSetMessage: increaseBoardSizeAndSetMessage,
        decreaseBoardSizeAndSetMessage: decreaseBoardSizeAndSetMessage,
        setMarkerAndChangeTurn: setMarkerAndChangeTurn,
        changeFirstPlayer: changeFirstPlayer,
        checkWinAndAddPoints: checkWinAndAddPoints,
        getTurn: getTurn,
        getFirstTurn: getFirstTurn,
        setFirstTurn: setFirstTurn,
        getBoard: getBoard,
        getIsFirstMoveDone: getIsFirstMoveDone,
        getIsGameStarted: getIsGameStarted
    };
})(BoardBuiderModule, HistoryModule, WinnerCheckerModule);
