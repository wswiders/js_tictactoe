describe('A GameModule',  function () {

    beforeEach(function() {
        spyOn(document, 'getElementById').and.callFake(function() {
            return document.createElement('div');
        });
        GameModule.setFirstTurn('X');
    });

    it('should call getBoardSize and getBoard when game is init',  function() {   
        //given
        var getBoardSize = spyOn(BoardBuiderModule, 'getBoardSize');
        var getBoard = spyOn(BoardBuiderModule, 'getBoard');
        //when
        GameModule.initGame();
        //then
        expect(getBoardSize).toHaveBeenCalled();
        expect(getBoard).toHaveBeenCalled();
    });

    it('should call increaseSizeAndSetMessage method BoardBuiderModule',  function() {   
        //given
        var increaseSizeAndSetMessage = spyOn(BoardBuiderModule, 'increaseSizeAndSetMessage');
        //when
        GameModule.increaseBoardSizeAndSetMessage();
        //then
        expect(increaseSizeAndSetMessage).toHaveBeenCalledWith(document.createElement('div'));
    });

    it('should call decreaseSizeAndSetMessage method from BoardBuiderModule',  function() {   
        //given
        var decreaseSizeAndSetMessage = spyOn(BoardBuiderModule, 'decreaseSizeAndSetMessage');
        //when
        GameModule.decreaseBoardSizeAndSetMessage();
        //then
        expect(decreaseSizeAndSetMessage).toHaveBeenCalledWith(document.createElement('div'));
    });

    it('should change first player turn',  function() {   
        //when
        GameModule.initGame();
        GameModule.changeFirstPlayer();
        //then
        expect(GameModule.getFirstTurn()).toEqual('O');
    });

    it('should change player turn and set X on board',  function() {   
        //given
        var square = document.createElement('div');
        square.id = '0';
        //when
        GameModule.initGame();
        GameModule.setMarkerAndChangeTurn(square);
        //then
        expect(GameModule.getTurn()).toEqual('O');
        expect(GameModule.getBoard()[0]).toEqual('X');
    });

    it('should not change player turn and set O on board when box is not empty',  function() {   
        //given
        var square = document.createElement('div');
        square.id = '0';
        //when
        GameModule.initGame();
        GameModule.setMarkerAndChangeTurn(square);
        GameModule.setMarkerAndChangeTurn(square);
        //then
        expect(GameModule.getTurn()).toEqual('O');
        expect(GameModule.getBoard()[0]).toEqual('X');
    });

    it('should player X have move in next turn and set X and O on board after two moves',  function() {   
        //given
        var square1 = document.createElement('div');
        square1.id = '0';
        var square2 = document.createElement('div');
        square2.id = '1';
        //when
        GameModule.initGame();
        GameModule.setMarkerAndChangeTurn(square1);
        GameModule.setMarkerAndChangeTurn(square2);
        //then
        expect(GameModule.getTurn()).toEqual('X');
        expect(GameModule.getBoard()[0]).toEqual('X');
        expect(GameModule.getBoard()[1]).toEqual('O');
    });

    it('should change flag when first move is done',  function() {   
        //given
        var square = document.createElement('div');
        square.id = '0';
        //when
        GameModule.initGame();
        GameModule.setMarkerAndChangeTurn(square);
        //then
        expect(GameModule.getIsFirstMoveDone()).toEqual(true);
    });

    it('should end game when X win',  function() {   
        //given
        var spyHistory = spyOn(HistoryModule, 'xWon');
        spyOn(WinnerCheckerModule, 'checkWinner').and.callFake(function() {
            return 'X';
        });
        //when
        GameModule.initGame();
        GameModule.checkWinAndAddPoints();
        //then
        expect(GameModule.getIsGameStarted()).toEqual(false);
    });

    it('should use correct method when X wins',  function() {   
        //given
        var spyHistory = spyOn(HistoryModule, 'xWon');
        spyOn(WinnerCheckerModule, 'checkWinner').and.callFake(function() {
            return 'X';
        });
        //when
        GameModule.initGame();
        GameModule.checkWinAndAddPoints();
        //then
        expect(spyHistory).toHaveBeenCalled();
    });

    it('should use correct method when O wins',  function() {   
        //given
        var spyHistory = spyOn(HistoryModule, 'oWon');
        spyOn(WinnerCheckerModule, 'checkWinner').and.callFake(function() {
            return 'O';
        });
        //when
        GameModule.initGame();
        GameModule.checkWinAndAddPoints();
        //then
        expect(spyHistory).toHaveBeenCalled();
    });

    it('should use correct method when O wins',  function() {   
        //given
        var spyHistory = spyOn(HistoryModule, 'wasDraw');
        spyOn(WinnerCheckerModule, 'checkWinner').and.callFake(function() {
            return '-';
        });
        //when
        GameModule.initGame();
        GameModule.checkWinAndAddPoints();
        //then
        expect(spyHistory).toHaveBeenCalled();
    });

    it('should not use history module when match still last',  function() {   
        //given
        var xWonHistory = spyOn(HistoryModule, 'xWon');
        var oWonHistory = spyOn(HistoryModule, 'oWon');
        var wasDrawHistory = spyOn(HistoryModule, 'wasDraw');
        spyOn(WinnerCheckerModule, 'checkWinner').and.callFake(function() {
            return '';
        });
        //when
        GameModule.initGame();
        GameModule.checkWinAndAddPoints();
        //then
        expect(xWonHistory).not.toHaveBeenCalled();
        expect(oWonHistory).not.toHaveBeenCalled();
        expect(wasDrawHistory).not.toHaveBeenCalled();
    });
});
