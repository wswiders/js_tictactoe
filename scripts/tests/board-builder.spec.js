describe('A BoardBuiderModule',  function () {

    beforeEach(function() {
        BoardBuiderModule.resetBoardSize();
    });

    it('should return board size equal 3',  function() {        
        expect(BoardBuiderModule.getBoardSize()).toEqual(3);   
    });

    it('should set min board size equal 3 and set correct message when try to decrease to lower value',  function() {        
        //given
        var textBox = document.createElement('div');
        //when
        BoardBuiderModule.decreaseSizeAndSetMessage(textBox);
        BoardBuiderModule.decreaseSizeAndSetMessage(textBox);
        //then
        expect(BoardBuiderModule.getBoardSize()).toEqual(3);
        expect(textBox.innerText).toEqual('Next round will sart with board size 3x3');
    });

    it('should set board size equal 5 and set correct message',  function() {        
        //given
        var textBox = document.createElement('div');
        //when
        BoardBuiderModule.increaseSizeAndSetMessage(textBox);
        BoardBuiderModule.increaseSizeAndSetMessage(textBox);
        //then
        expect(BoardBuiderModule.getBoardSize()).toEqual(5);
        expect(textBox.innerText).toEqual('Next round will sart with board size 5x5');
    });

    it('should set max board size equal 7 and set correct message when try to decrease to lower value',  function() {        
        //given
        var textBox = document.createElement('div');
        //when
        BoardBuiderModule.increaseSizeAndSetMessage(textBox);
        BoardBuiderModule.increaseSizeAndSetMessage(textBox);
        BoardBuiderModule.increaseSizeAndSetMessage(textBox);
        BoardBuiderModule.increaseSizeAndSetMessage(textBox);
        BoardBuiderModule.increaseSizeAndSetMessage(textBox);
        BoardBuiderModule.increaseSizeAndSetMessage(textBox);
        //then
        expect(BoardBuiderModule.getBoardSize()).toEqual(7);
        expect(textBox.innerText).toEqual('Next round will sart with board size 7x7');
    });

    it('should return board with empty strings',  function() {
      //when
        var actual = BoardBuiderModule.getBoard();
        //then
        expect(actual.length).toEqual(9);
    });
});
