describe('A WinnerCheckerModule', function() {

    it('should return X when player X won match', function() {
        // given
        var boardSize = 3;
        var board = getEmptyBoard(boardSize);
        board[0] = 'X';
        board[3] = 'O';
        board[1] = 'X';
        board[6] = 'O';
        board[2] = 'X';
        // when
        var actual = WinnerCheckerModule.checkWinner(boardSize, board);
        // then
        expect(actual).toEqual('X');
    });

    it('should return O when player O won match', function() {
        // given
        var boardSize = 3;
        var board = getEmptyBoard(boardSize);
        board[0] = 'O';
        board[3] = 'O';
        board[6] = 'O';
        // when
        var actual = WinnerCheckerModule.checkWinner(boardSize, board);
        // then
        expect(actual).toEqual('O');
    });

    it('should return "-" when match ended in draw', function() {
        // given
        var boardSize = 3;
        var board = getEmptyBoard(boardSize);
        board[0] = 'X';
        board[3] = 'O';
        board[1] = 'X';
        board[4] = 'O';
        board[2] = 'O';
        board[5] = 'X';
        board[7] = 'O';
        board[8] = 'X';
        board[6] = 'X';
        // when
        var actual = WinnerCheckerModule.checkWinner(boardSize, board);
        // then
        expect(actual).toEqual('-');
    });

    it('should return "" when match still last', function() {
        // given
        var boardSize = 3;
        var board = getEmptyBoard(boardSize);
        board[0] = 'X';
        // when
        var actual = WinnerCheckerModule.checkWinner(boardSize, board);
        // then
        expect(actual).toEqual('');
    });

    it('should return X when player X won match on bigger board', function() {
        // given
        var boardSize = 4;
        var board = getEmptyBoard(boardSize);
        board[0] = 'X';
        board[4] = 'O';
        board[1] = 'X';
        board[5] = 'O';
        board[2] = 'X';
        board[6] = 'O';
        board[3] = 'X';
        // when
        var actual = WinnerCheckerModule.checkWinner(boardSize, board);
        // then
        expect(actual).toEqual('X');
    });

    function getEmptyBoard(boardSize) {
        var board = [];
        for (var i = 0; i < boardSize; i++) {
            for (var j = 0; j < boardSize; j++) {
                board[i * boardSize + j] = '';
            }
        }
        return board;
    }
});
