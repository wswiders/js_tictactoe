describe('A HistoryModule',  function () {

    it('should set statistic to 0',  function() {        
        expect(HistoryModule.getTimesOWon()).toEqual(0);        
        expect(HistoryModule.getTimesXWon()).toEqual(0);        
        expect(HistoryModule.getTimesWasDraw()).toEqual(0);    
    });
});
